
docker container prune -f
docker image prune --all -f

# Load the .env file
get-content .env | foreach {
    $name, $value = $_.split('=')
    set-content env:\$name $value
}

docker login registry.gitlab.com
docker build -t registry.gitlab.com/serialopus/test-image/build-windows:latest --build-arg QT_EMAIL=$Env:QT_EMAIL --build-arg QT_PASSWORD=$env:QT_PASSWORD -f Dockerfile_windows .
docker push registry.gitlab.com/serialopus/test-image/build-windows
